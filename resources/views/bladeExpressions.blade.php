
@push('scripts')
<script src="/example.js"></script>
@endpush

{{ $name }}
<br>
{{ isset($name) ? $name : 'Default' }}
<br>
{{ $name or 'Default' }}
<br>
Hello, {!! $name !!}.
<br>
{!! $name !!}
<br>

// blade && javascript render
<h1>Laravel</h1>

Hello, @{{ $name }}.

@verbatim
<div class="container">
    Hello, {{ $name }}.
</div>
@endverbatim

@unless($records)
    I don't have any records!
@endunless

@if (count($records) === 1)
    I have one record!
@elseif (count($records) > 1)
    I have multiple records!
@endif

{{-- This comment will not be present in the rendered HTML --}}

@each('view.name', $records, ' records')

@stack('scripts')
