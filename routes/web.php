<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//


Route::get('/','homeController@index');
Route::get('mobile','mobileController@index');
Route::get('roles','roleController@index');
Route::get('hasmanythrough','CountryController@index');
Route::resource('songs','SongsController');


Route::get('about', function () {
    $tests=['This',' is',' from', ' totapakhi'];
    return view('about', compact('tests'));
//    return view('about')->withtests($tests);
//    return view('about')->with('tests', $tests);
//    return view('about',['tests'=>$tests]);
});

Route::get('bladeExpressions', function () {
    $name='Totapakhi';
    $records=[1, 2, 3, 4];
    $records=[];
    return view('bladeExpressions', compact(['name', 'records']));
});


Route::get('app', function () {
    return view('app');
});
Route::get('app2', function () {
    return view('app2');
});