<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;

class roleController extends Controller
{
    public function index(){
        $roles=Role::find(3)->user;
        return view('roles.index', compact('roles'));
    }
}
