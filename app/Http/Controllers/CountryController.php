<?php

namespace App\Http\Controllers;

use App\Country;
use App\Post;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function index(){
        $countrys=Country::find(1)->posts;
        return view('hasmanythrough.index', compact('countrys'));
    }
}
