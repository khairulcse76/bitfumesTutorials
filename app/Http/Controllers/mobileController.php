<?php

namespace App\Http\Controllers;

use App\Mobile;
use App\User;
use Illuminate\Http\Request;

class mobileController extends Controller
{
    public function index(){
        $mobiles=User::find(2)->mobile;

        return view('mobile', compact('mobiles'));
    }
}
