<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\home;

class homeController extends Controller
{
   public function index(){
       $user=User::find(1);
       return view('welcome', compact('user'));
   }
}
